# SalesBot APi

[Tutorial On Setting this Up with Go High Level](https://docs.google.com/document/d/1BDG11kyUJX5UKpeAUS_3Y1XCy2oD_Xc8_lpUullCBeA/edit?usp=sharing)

## Getting started

This API allows you to send a **POST** request to the endpoint `https://xljmafyych.execute-api.us-west-2.amazonaws.com/message/` with the provided parameters to interact with your custom SalesBot.  This SalesBot is built on top of the newest ChatGPT-3 technology with dynamic prompts and logic to enhance its ability to carry on conversation with the lead and track which of your custom goals has been accomplished, stopping the conversation when all goals have been realized.

## Example Request

```
curl --location --request POST 'https://xljmafyych.execute-api.us-west-2.amazonaws.com/message/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "phone":     "0000000000",
    "api_key":   "3c40845b-389c-46dc-bbvd-f4f9a928257e",
    "ghl_key":   "f4f9a928257ef4f9a928257ef4f9a928257ef4f9a928257e"
    "message":   "I'\''m interested in working with your company",
    "lead_name": "Tom",
    "rep_name":  "Sam",
    "meta":      "{'\''@offer'\'': '\''$400,000'\''}",
    "extra_prompt": "For this message I want you to act with the voice of darth vader"
}'
```
**Bold keys are required inputs for the API**

- **phone**       = phone number or other unique identifier at least 10 digits long for the contact

- **api_key**     = api key specific to your account

- **message**     = the message from the contact

- **rep_name**    = the name of your SalesBot

- **lead_name**   = the name of the lead you're interacting with

- goals_finished  = list of goals already completed to override the goals SalesBot has in memory

- ghl_key         = API key for your Go High Level based CRM (REIReply, Leadwire, etc.). Will update your contacts without the need for a zapier or other similar account (See link above for setup)

- limit           = limit the number of automated messages SalesBot will have with the lead

- meta            = additional variables and their values to fill in in the bot's training

- extra_prompt    = additional prompt to add to the user-specified training prompt already loaded


## Responses

```JSON
{
    "message":  "Hi Tom, this is Sam with CashOffers, I think we would be a great fit.  Can you tell me a bit more about the condition?",
    "goals":    {
        "finished": ["motivation"],
        "current":  "condition",
        "success":  "motivation",
        "notes":    ""
    },
    "continue": "yes"
}

message            = the message generated from the bot that should be sent to the lead
finished           = goals tha the bot has declared as being finished
current            = the current goal in work
success            = any goal that was marked as completed during this message cycle
notes              = additional notes about this message iteration
continue           = whether or not the bot plans on continuing from here based on limits, goals, completed, etc (yes or no)
```

## Status Codes

- 200:  Successful return of SalesBot message
- 201:  Detected identical message received within 2 seconds of another same message body so it will be ignored
- 202:  Lead has sent the code word 'reset' to reset the message history (used for testing)
- 205:  The goals are all complete and the bot is done responding
- 210:  You are out of credits in your account
- 400:  Error occured (see message body for details)

## Flow

To better track the conversation all inbound and outbound messages are tracked in our memory.  This means, if you're not sending a message to the API, it won't be tracked.  Currently there is no way to tell the API what your previous conversation history has been like with the target lead.  This API is best used for new leads from the start of their conversation history.  Alternatively, you can use the "goals_finished" key and manually tell the API which goals have already been accomplished for the target lead and the bot will start trying to satisfy other goals in your profile that are not selected.  If you are testing the system and want to reset your conversation history to try again from the beginning, just send **reset** in the message value and the conversation will be cleared.

## Credits

Credits cost $0.05/credit and each automated message uses 1 credit.  Currently you must handle payment for credits directly with Bryce.  Your user portal is in work.  Once this is complete you will be able to monitor and top off your credits here!

## Monthly cost

The API access costs $199/month.  This gives you access to the exclusive Slack Channel for help and support and customization requests for your profile

## Building Your profile

Eventually these values will be able to be modified from your user portal.  For now, they can be modified by contacting Bryce.

1. Description about your business, services you offer and business name
2. Goals you want the bot to accomplish and the order in which it should try to accomplish them (example:  determine motivation, sales price, condition)
3. The voice you'd like the bot to use.  examples: friendly, forward, professional, unprofessional, caring, etc.

## Meta and Other variables

Variables that are used when training your bot that are hard set for use are

- @lead_name - The value from your request lead_name
- @rep_name - The value from your request rep_name

You can set custom variables that you want to use in your **meta** key.  Examples would be

- @offer_amount - $400,000
- @closing_days - 14

Let's say you had some of these variables in your **extra_prompt** key.  This is how that information would be updated during the bot training for that message automation step.  This is where you can change your bot training depending on the step that lead is at in the conversion process.

_Tell @lead_name that @rep_name is able to offer @offer_amount and close in @closing_days days_

Tell Tom that Sam is able to offer $400,000 and close in 14 days


